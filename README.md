# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 03: Blinky con SysTick

En este ejemplo se configura el SysTick para hacer un delay preciso en la alternancia del LED.

Entrada del blog: [Tiempo de debugging con el STM32F103](https://electronlinux.wordpress.com/2020/06/11/tiempo-de-debugging-con-el-stm32f103/)

Video 1 en YouTube: [Configuración del SysTick del ARM Cortex-M3 con PlatfromIO y libOpenCM3](https://youtu.be/gmQsAweZrA0)

Video 2 en YouTube: [Utilizando el Debugger en PlatfromIO con la BluePill](https://youtu.be/H-4cSMZ6hYQ)

