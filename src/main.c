/**
 * @file main.c
 * @author Matías S. Ávalos (@tute_avalos)
 * @brief Ejemplo de uso del Systick
 * @version 0.2
 * @date 2020-09-07
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */
#include<libopencm3/stm32/rcc.h>
#include<libopencm3/stm32/gpio.h>
#include<libopencm3/cm3/systick.h>
#include<libopencm3/cm3/nvic.h>

volatile uint32_t millis; ///< @brief ticks de la última cuenta (cada 1ms)

/**
 * @brief Delay bloqueante utilizando el SysTick
 * 
 * @param ms duración de la demora
 */
void delay_ms(uint32_t ms);

int main(void)
{
    // SYSCLK a 72Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // PC13 (LED de la BluePill) como salida:
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

    /*** Configuración del SysTyck ***/
    // Se toma la velocidad del AHB (SYSCLK) y se activa el divisor por 8:
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8); // 72M/8 = 9M

    // Se carga el valor a cargar cuando el SysTick llega a 0 (downflow):
    systick_set_reload(8999); // 9M / 9000 = 1k => T = 1ms
    
    // Se habilita la interrupción del SysTick:
    systick_interrupt_enable();
    
    // El SysTick empieza a contar:
    systick_counter_enable();

    while(true)
    {
        // Se alterna el LED en PC13
        gpio_toggle(GPIOC,GPIO13);
        // Esperamos 500ms
        delay_ms(500);
    }
}

void delay_ms(uint32_t ms)
{
    uint32_t tm = millis + ms;
    while(millis < tm);
}

/**
 * @brief Sub-Rutina de interrupción del SysTick (cada 1ms)
 */
void sys_tick_handler(void)
{
    millis++; // Se incrementan los millis
}
